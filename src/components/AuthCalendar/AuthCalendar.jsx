import React, { useState } from "react"
import {createBrowserHistory} from 'history'
import axios from 'axios';
import Loader from "react-loader-spinner";
import style from './AuthCalendar.module.css';
import { styleNames } from "../../utils/style-names";
const sn = styleNames(style);

export default function AuthCalendar  ()  {
  const [auth, setAuth] = useState(false)
  const [loading, setLoading] = useState(false)
  const CLIENT_ID =  process.env.REACT_APP_GOOGLE_CLIENT_ID;
  const API_KEY =  process.env.REACT_APP_API_KEY;
  const search = createBrowserHistory().location.search; // * текущий параметр
  const companyId = search.replace('?companyId=', '');

  function authenticate() {
    return window.gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly"})
        .then((googleUser)=>  {   
        // ? метод возвращает объект пользователя где есть все необходимые нам поля
        const profile = googleUser.getBasicProfile()
        // токен
        const id_token = googleUser.getAuthResponse().id_token
        const authResponse = googleUser.getAuthResponse()
        localStorage.setItem('token', id_token); // * запись токена в localStorage

        const userData = {id: profile.getId(),
          fullName: profile.getName(),
          givenName: profile.getGivenName(),
          familyName: profile.getFamilyName(),
          imageUrl: profile.getImageUrl(),
          email: profile.getEmail(),
          id_token: id_token,
          dataAuth: authResponse,
          companyId: companyId
      }
       return userData
      })
  }

  function loadClient(userData) {
    window.gapi.client.setApiKey(API_KEY);
    window.gapi.client.load("https://content.googleapis.com/discovery/v1/apis/calendar/v3/rest")
        .then(function() { 
          postCalendar(userData) 
      }).catch(error=>{
        setAuth(false)
        setLoading(false)
        console.error("Error loading GAPI client for API", error);});
  }

  function postCalendar(userData) {
    setLoading(true)
    return window.gapi.client.calendar.calendars.insert({
      "resource": {
        "summary": "ChatBot-Записи"
      }
    })
        .then(function(response) {
          setLoading(false)        
            axios.post(`https://webhook.site/b7329485-2e38-43d3-860d-d52c8798a8a9`, {user: userData,
        calendar: response.result})
        setLoading(false)
        setAuth(true)
              }).catch(error => {
                setLoading(false)
                setAuth(false)
                console.error('error postCalendar: ', error)});
  }

  window.gapi.load("client:auth2", function() {
    window.gapi.auth2.init({client_id: CLIENT_ID}).then(
              ()=>{console.log('init ok')
            },()=>{console.log('init err')
            });
  });

const onAuthAndPostCalendar = ()=>authenticate().then(loadClient).catch(err=>{
  setAuth(false)
  setLoading(false)
  console.error("Error signing in", err);})
 
    return(<div className={style.container}>
        <div className={sn('row', {'hidden': loading})}>
  <div className="col-md-3">
    <button className="btn btn-outline-dark" onClick={()=>onAuthAndPostCalendar()} style={{textTransform:'none'}}>
      <img width="20px" style={{marginBottom:'3px', marginRight:'5px'}} alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
      Увійдіть через Google
    </button>
  </div>
</div>
       {loading && <Loader
        type="TailSpin"
        color="#00BFFF"
        height={100}
        width={100}
        timeout={6000} //6 secs
      />}
        {auth && <div className="jumbotron text-center">
  <h1 className="display-3">Thank You!</h1>
  <p className="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
  <hr />
  <p>
    Having trouble? <a href="/">Contact us</a>
  </p>
  <p className="lead">
    <a className="btn btn-primary btn-sm" href="/">Continue to homepage</a>
  </p>
</div>}
              </div>)
}
