import './App.css';
import AuthCalendar from './components/AuthCalendar/AuthCalendar';

function App() {
  return (
    <div>
      <AuthCalendar/>
    </div>
  );
}

export default App;
